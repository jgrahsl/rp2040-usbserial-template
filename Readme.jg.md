rustup target add thumbv6m-none-eabi
apt install libudev-dev
cargo install flip-link
cargo install probe-rs --features=cli --locked
cargo run

Further details https://github.com/rp-rs/rp2040-project-template/blob/main/README.md

#![allow(dead_code)]
#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unused_variables)]
#![allow(unused_imports)]

#[derive(Clone, Copy)]
#[repr(u8)]
enum AD7793Regs {
    //Control16 = 0x80,
    STATUS_R = 0x0,
    MODE_16 = 0x1,
    CONF_16 = 0x2,
    DATA_24 = 0x3,
    ID = 0x4,
    IO = 0x5,
    OFFSET_24 = 0x6,
    FULL_24 = 0x7,
}

pub enum Status {
    Channel0 = 0x01,
    Channel1 = 0x02,
    Channel2 = 0x04,
    Error = 0x40,
    Ready = 0x80,
}

pub enum Mode {
    Continuous = 0x0,
    Single = 0x1,
    Idle = 0x2,
    Powerdown = 0x3,
    InternalZero = 0x4,
    InternalFull = 0x5,
    SystemZero = 0x6,
    SystemFull = 0x7,
}

enum Mode2 {
    UpdateRate = 0xf, // 500ms conv cycle
    ClockRef = 0x0,   // Internal clock and output on CLK pin
}

pub enum Reg {
    Comm8,
    Status8,
    Mode16,
    Conf16,
    Data24,
    Id8,
    Io8,
    Offset24,
    Full24,
}

const OPCODE_REG_MASK: u8 = 0x7; // Reg id must be >= 0
const OPCODE_REG_OFFSET: isize = 3;
const COMM_RNW_BIT_MASK: u8 = 0x40;

const HALF_DUPLEX_DUMMY_BYTE: u8 = 0x00; // Bytes that are put into spi write buffers for half duplex devices

const CHIP_ID_LOW_NIBBLE: u8 = 0x0b; // The ID that is return by the ID_8 register, only observe the lower nibble

impl Into<u8> for Reg {
    fn into(self) -> u8 {
        match self {
            Reg::Comm8 => 0x88 << OPCODE_REG_OFFSET,
            Reg::Status8 => 0x0 << OPCODE_REG_OFFSET,
            Reg::Mode16 => 0x1 << OPCODE_REG_OFFSET,
            Reg::Conf16 => 0x2 << OPCODE_REG_OFFSET,
            Reg::Data24 => 0x3 << OPCODE_REG_OFFSET,
            Reg::Id8 => 0x4 << OPCODE_REG_OFFSET,
            Reg::Io8 => 0x5 << OPCODE_REG_OFFSET,
            Reg::Offset24 => 0x6 << OPCODE_REG_OFFSET,
            Reg::Full24 => 0x7 << OPCODE_REG_OFFSET,
        }
    }
}
////////////////////////

use rp_pico::hal::spi::Spi;
use rp_pico::hal::spi::ValidSpiPinout;
use rp_pico::hal::spi::Enabled;
use rp_pico::hal::spi::SpiDevice;

pub trait SpiDelegate
{
    fn transfer<'w>(&mut self, data : &'w mut [u8]) -> &'w [u8];
}

pub struct CN0326<T>
where
{
    dev : T
}

impl<T> CN0326<T>
where
T : SpiDelegate
{
    pub fn new(d : T) -> Self {
        Self { dev: d }
    }

    fn read_reg(&mut self, reg: AD7793Regs) -> Result<u8, ()> {
        let mut register_command: [u8; 2] = [0; 2];
        
        register_command[0] = 0x00 | 0x40 | ((reg as u8) << 3) | 0x00 | 0x00;

        let _ = self.dev.transfer(&mut register_command);

        return Ok(register_command[1]);
    }

    fn read_reg8_val8(&mut self, reg: Reg) -> u8 {
        const WR_LEN: usize = 1;
        const RD_LEN: usize = 1;
        const WR_OFFSET: usize = 0;
        const RD_OFFSET: usize = WR_LEN;
        const LEN: usize = WR_LEN + RD_LEN;

        let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];

        write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

        self.dev.transfer(&mut write_buf);

        return (write_buf[RD_OFFSET + 0] as u8) << 0;
    }

    fn write_reg8_val8(&mut self, reg: Reg, val: u8) {
        const WR_LEN: usize = 2;
        const LEN: usize = WR_LEN;

        let mut write_buf: [u8; LEN] = [reg.into(), ((val >> 0) & 0xff) as u8];

        self.dev.transfer(&mut write_buf);
    }

    fn read_reg8_val16(&mut self, reg: Reg) -> u16 {
        const WR_LEN: usize = 1;
        const RD_LEN: usize = 2;
        const WR_OFFSET: usize = 0;
        const RD_OFFSET: usize = WR_LEN;
        const LEN: usize = WR_LEN + RD_LEN;

        let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];

        write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

        self.dev.transfer(&mut write_buf);

        return ((write_buf[RD_OFFSET + 0] as u16) << 8)
            | ((write_buf[RD_OFFSET + 1] as u16) << 0);
    }

    fn write_reg8_val16(&mut self, reg: Reg, val: u16) {
        const WR_LEN: usize = 3;
        const WR_OFFSET: usize = 0;
        const LEN: usize = WR_LEN;

        let mut write_buf: [u8; LEN] = [
            reg.into(),
            ((val >> 8) & 0xff) as u8,
            ((val >> 0) & 0xff) as u8,
        ];

        self.dev.transfer(&mut write_buf);
    }

    fn read_reg8_val24(&mut self, reg: Reg) -> u32 {
        const WR_LEN: usize = 1;
        const RD_LEN: usize = 3;
        const WR_OFFSET: usize = 0;
        const RD_OFFSET: usize = WR_LEN;
        const LEN: usize = WR_LEN + RD_LEN;

        let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];

        write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

        self.dev.transfer(&mut write_buf);

        return ((write_buf[RD_OFFSET + 0] as u32) << 16)
            | ((write_buf[RD_OFFSET + 1] as u32) << 8)
            | ((write_buf[RD_OFFSET + 2] as u32) << 0);
    }

    fn write_reg8_val24(&mut self, reg: Reg, val: u32) {
        const WR_LEN: usize = 4;
        const WR_OFFSET: usize = 0;
        const LEN: usize = WR_LEN;

        let mut write_buf: [u8; LEN] = [
            reg.into(),
            ((val >> 16) & 0xff) as u8,
            ((val >> 8) & 0xff) as u8,
            ((val >> 0) & 0xff) as u8,
        ];

        let _ = self.dev.transfer(&mut write_buf);

    }

    fn reset_device(&mut self) {
        let mut write_buf: [u8; 4] = [0xff, 0xff, 0xff, 0xff];

        let _ = self.dev.transfer(&mut write_buf);

        assert_eq!(self.read_reg8_val8(Reg::Id8) & 0xf, CHIP_ID_LOW_NIBBLE);
        // ID must be 0xXb;
    }

    fn set_io(&mut self) {
        const REG: Reg = Reg::Io8;

        // Bitffield content
        const IEXCDIR: u8 = 0x0;
        const IEXCEN: u8 = 0x2; // 210uA current source hits a 5k/5k voltage divider biasing the pH probe

        let v = (IEXCDIR << 2) | (IEXCEN << 0);

        self.write_reg8_val8(REG, v);
        assert_eq!(self.read_reg8_val8(REG), v);
    }

    fn set_conf(&mut self, ch: u16) {
        const REG: Reg = Reg::Conf16;

        // Bitffield content
        const BUFFER_INPUT: u16 = 0; // # unbuffered adc input for use with gain 1x
        const REF_SEL: u16 = 0; // # extern reference taken from REFIN1+ (== 2.1V / 2) driven by 210uA current source over 5k/5k voltage divider
        const GAIN: u16 = 0; // # 1x gain
        const BOOST: u16 = 0; // # no boosting of bias since not in use
        const BIAS: u16 = 0; // no bias
        const POLARITY: u16 = 0; // # Bipolar operation

        let v: u16 = ch
            | (BUFFER_INPUT << 4)
            | (REF_SEL << 7)
            | (GAIN << 8)
            | (BOOST << 11)
            | (POLARITY << 12)
            | (BIAS << 14);

        self.write_reg8_val16(REG, v);
        assert_eq!(self.read_reg8_val16(REG), v);
    }

    fn set_mode(&mut self, mode: Mode) {
        const REG: Reg = Reg::Mode16;

        const UPDATE_RATE: u16 = 0b1111; // f_adc = 4.17hz, settling time 480ms
        const CLOCK_REF: u16 = 0x0; // Use internal 64khz clock and no output on clk pin

        let v = UPDATE_RATE | (CLOCK_REF << 6) | ((mode as u16) << 13);

        self.write_reg8_val16(REG, v);
        assert_eq!(self.read_reg8_val16(REG), v);
    }

    fn wait_data_ready(&mut self) -> Result<(), ()> {
        loop {
            let status = self.read_reg8_val8(Reg::Status8);

            if (status & (Status::Error as u8)) != 0 {
                return Err(());
            }
            if (status & (Status::Ready as u8)) == 0 {
                return Ok(());
            }
        }
    }

    fn read_full_scale(&mut self) -> u32 {
        self.read_reg8_val24(Reg::Full24)
    }

    fn write_full_scale(&mut self, val: u32) {
        self.write_reg8_val24(Reg::Full24, val);
    }

    fn read_data(&mut self) -> u32 {
        self.read_reg8_val24(Reg::Data24)
    }

    pub fn measure_uv(&mut self) -> f64 {
        self.reset_device();

        self.set_io();
        self.set_conf(0);
        self.set_mode(Mode::Idle);

//          std::thread::sleep(std::time::Duration::from_millis(150));

        // Calibrate for Value Zero, result is stored internally in the ADC
        //d.set_conf(3); // For Mode::SystemZero use channel 3 which is AIN1- vs AIN1- (== short)
        self.set_mode(Mode::InternalZero);
        self.wait_data_ready()
            .expect("Error while Internal Zero Calibration");

        // Calibrate for Value Max, result is stored internally in the ADC
        self.set_mode(Mode::InternalFull);
        self.wait_data_ready()
            .expect("Error while Internal Full Calibration");

        // Measure actual pH probe and read Data
        self.set_conf(0); // AIN1+ vs AIN1- is the pH Probe
        self.set_mode(Mode::Single);
        self.wait_data_ready().expect("Error while ADC measurement");

        let raw = self.read_data() as f64;

        let u_volt = (raw - 0x80_0000 as f64) // Convert register content to signed float
                 /
                 (0x100_0000 as f64)
            * 2_100_000.0; // 2.1V resulting from 210uA current through a 5k/5k(==10k) voltage divider going into REFIN1+

        u_volt
    }


    pub fn measure_ph(&mut self, temperature : f64) -> f64 {

        // TODO describe model and assumptions behind calibration
        let calib : ProbeCalibration = ProbeCalibration::default();

        7.0 - (((self.measure_uv() - calib.calibration_ph7_in_uv) / (calib.nerst_factor + temperature * calib.nerst_deviate)) * calib.calibration_probe_scale)
    }

}

//#[builder(default)]
//#[derive(derive_builder::Builder, Debug, Serialize, Deserialize)]
pub struct ProbeCalibration {
    name : &'static str,
    // Values that are specific to the type of probe
    nerst_factor : f64,
    nerst_deviate : f64,

    // Values that are specific to the particular probe
    // Place probe into 7.0 buffer and record the uV ADC reading here:
    calibration_ph7_in_uv : f64,
    // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
    calibration_probe_scale : f64
}

impl Default for  ProbeCalibration {
    fn default() -> ProbeCalibration {
        ProbeCalibration {
            name : "white",
            // Values that are specific to the type of probe
            nerst_factor : 54.20 * 1_000.0, // uV per pH at 0 Celsius
            nerst_deviate : 0.1984 * 1_000.0, // uV per pH per degree celsius


            // Values that are specific to the particular probe
            // Place probe into 7.0 buffer and record the uV ADC reading here:
            calibration_ph7_in_uv : -46_200.1, // uV ADC reading at pH7.0
            // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
            calibration_probe_scale : ((7.00 - 4.00) / (7.00 - 4.8)) // Specific probe scale factor
        }
    }
}


#![no_std]
#![no_main]

use embedded_hal::prelude::_embedded_hal_blocking_spi_Transfer;
use embedded_hal::spi::*;
// use embedded_time::rate::{*};

// The macro for our start-up function
use rp_pico::{entry, hal::gpio::FunctionSpi};

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
use panic_halt as _;

// A shorter alias for the Peripheral Access Crate, which provides low-level
// register access
use rp_pico::hal;

use defmt::*;
use defmt_rtt as _;

// A shorter alias for the Hardware Abstraction Layer, which provides
// higher-level drivers.

// USB Device support
use usb_device::{class_prelude::*, prelude::*};

// USB Communications Class Device support
use usbd_serial::SerialPort;

// Used to demonstrate writing formatted strings
use core::fmt::Write;
use heapless::String;
use heapless::spsc;

use fugit::RateExtU32;
//mod cn0326;
//use cn0326::CN0326;
//
/// Entry point to our bare-metal application.
///
/// The `#[entry]` macro ensures the Cortex-M start-up code calls this function
/// as soon as all global variables are initialised.
///
/// The function configures the RP2040 peripherals, then echoes any characters
/// received over USB Serial.
use rp2040_hal::spi;
use rp2040_hal::Clock;
use rp2040_hal::Spi;

mod cn0326;

struct LocalSpi<D, P>
where
    D: spi::SpiDevice,
    P: rp2040_hal::spi::ValidSpiPinout<D>,
{
    pub dev: Spi<spi::Enabled, D, P, 8>,
}

impl<D, P> cn0326::SpiDelegate for LocalSpi<D, P>
where
    D: spi::SpiDevice,
    P: rp2040_hal::spi::ValidSpiPinout<D>,
{
    fn transfer<'w>(&mut self, words: &'w mut [u8]) -> &'w [u8] {
        if let Ok(ret) = self.dev.transfer(words) {
            return ret;
        } else {
            defmt::panic!();
        }
    }
}

static mut CORE1_STACK: hal::multicore::Stack<4096> = hal::multicore::Stack::new();
static mut queue : spsc::Queue<f64, 16> = spsc::Queue::new();

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut sio = hal::Sio::new(pac.SIO);
    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut _led_pin = pins.led.into_push_pull_output();

    let _sys_freq = clocks.system_clock.freq().to_Hz();

    let sclk = pins.gpio2.into_function::<FunctionSpi>();
    let mosi = pins.gpio3.into_function::<FunctionSpi>();
    let miso = pins.gpio4.into_function::<FunctionSpi>();
    let _csn = pins.gpio5.into_function::<FunctionSpi>();
    // SPI
    let spi = hal::spi::Spi::<_, _, _, 8>::new(pac.SPI0, (mosi, miso, sclk)).init(
        &mut pac.RESETS,
        125_000_000.Hz(),
        100_000.Hz(),
        &MODE_3,
    );

    let delegate = LocalSpi { dev: spi };

    let mut ph = cn0326::CN0326::new(delegate);

    let (mut send, mut recv) = unsafe { queue.split() }; 

    // Timer
    let timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);
    let mut said_hello = false;

    /////////////////////////////////////////////////////////////////////////////////////
    let mut mark: u64;

    let mut mc = hal::multicore::Multicore::new(&mut pac.PSM, &mut pac.PPB, &mut sio.fifo);
    let cores = mc.cores();
    let core1 = &mut cores[1];

    let _test = core1.spawn(unsafe { &mut CORE1_STACK.mem }, move || {
        // USB
        // Set up the USB driver
        let usb_bus = UsbBusAllocator::new(hal::usb::UsbBus::new(
            pac.USBCTRL_REGS,
            pac.USBCTRL_DPRAM,
            clocks.usb_clock,
            true,
            &mut pac.RESETS,
        ));

        // Set up the USB Communications Class Device driver
        let mut serial = SerialPort::new(&usb_bus);

        // Create a USB device with a fake VID and PID
        let mut usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
            .manufacturer("Fake company")
            .product("Serial port")
            .serial_number("TEST")
            .device_class(2) // from: https://www.usb.org/defined-class-codes
            .build();

        let pac2 = unsafe { hal::pac::Peripherals::steal() };
        let mut sio2 = hal::Sio::new(pac2.SIO);

        loop {

           let payload = recv.dequeue();
//         let input = sio2.fifo.read();
//         sio.fifo.write_blocking();

           match payload {
              Some(f64) => { 
                  let mut buf: String<128> = String::new();
                  writeln!(&mut buf, "{{\"source\" : \"cn0326\", \"pH_uv\" : {}\"}}\r", f64).unwrap();
                  
                  let mut wr_ptr = buf.as_bytes();
                  while !wr_ptr.is_empty() {
                      match serial.write(wr_ptr) {
                          Ok(len) => wr_ptr = &wr_ptr[len..],
                          // On error, just drop unwritten data.
                          // One possible error is Err(WouldBlock), meaning the USB
                          // write buffer is full.
                          Err(_) => break,
                      };
                  }
              },
              None => { /* sleep */ },
           };

           if usb_dev.poll(&mut [&mut serial]) {
               let mut buf = [0u8; 64];
               match serial.read(&mut buf) {
                   Err(_e) => {
                       // Do nothing
                   }
                   Ok(0) => {
                       // Do nothing
                   }
                   Ok(_count) => {
                       // Do nothing
                   }
               }
           }
        }
    });

    info!("Looping (info!):\n");

    loop {
        let current_tick = timer.get_counter().ticks();

        // A welcome message at the beginning
        if !said_hello && current_tick >= 2_500_000 {
            said_hello = true;
        }


        mark = current_tick;
        let uv = ph.measure_uv();
        info!("measurement took {} ms and returned: {} uV", (timer.get_counter().ticks() - mark) / 1_000, uv as u32);
        //sio.fifo.write_blocking(uv as u32);
        send.enqueue(uv);
    }
}

// End of file
